# Edit this configuration file to define what should be installed on
# your system. Help is available in the configuration.nix(5) man page, on
# https://search.nixos.org/options and in the NixOS manual (`nixos-help`).

{ config, lib, pkgs, ... }:

{
  # List packages installed in system profile. To search, run:
  # $ nix search wget
  environment.systemPackages = with pkgs; [
    vim # Do not forget to add an editor to edit configuration.nix! The Nano editor is also installed by default.
    wget git btop lshw tree firefox
    kitty dmenu screen xscreensaver yarn nodejs
    brightnessctl duf nitch neofetch nyancat
    dunst flameshot gucharmap networkmanagerapplet nitrogen
    xmonad-log gnome.nautilus unzip rclone mkpasswd
    keybase keybase-gui kbfs google-chrome polybar
    xorg.xmodmap pasystray rofi xcompmgr dialog alacritty
    ranger bottom
  ];

  nixpkgs.config.packageOverrides = pkgs: {
    rofi = pkgs.rofi.override {
      plugins = [ pkgs.rofi-calc pkgs.rofi-emoji ];
    };
  };

}

