# Edit this configuration file to define what should be installed on
# your system. Help is available in the configuration.nix(5) man page, on
# https://search.nixos.org/options and in the NixOS manual (`nixos-help`).

{ config, lib, pkgs, ... }:

{
  imports =
    [ # Include the results of the hardware scan.
      ./hardware-configuration.nix
      # Setup the base install packages
      ./packages-configuration.nix
      # Include the X11 components
      ./wm/xmonad.nix
    ];

  nixpkgs.config.allowUnfree = true;
  nix.settings.experimental-features = [ "nix-command" "flakes" ];
  powerManagement.enable = true;
  systemd.targets.sleep.enable = false;
  systemd.targets.suspend.enable = false;
  systemd.targets.hibernate.enable = false;
  systemd.targets.hybrid-sleep.enable = false;

  # Use the systemd-boot EFI boot loader.
  boot.loader.systemd-boot.enable = true;
  boot.loader.efi.canTouchEfiVariables = false;

  networking.hostName = "tortoise"; # Define your hostname.
  # Pick only one of the below networking options.
  # networking.wireless.enable = true;  # Enables wireless support via wpa_supplicant.
  networking.networkmanager.enable = false;  # Easiest to use and most distros use this by default.
#  networking.wireless.iwd = {
#    enable = true;
#    settings.General.EnableNetworkConfiguration = true;
#  };
  networking.interfaces.enp2s0.useDHCP = true;

  # Set your time zone.
  time.timeZone = "America/Los_Angeles";

  # enable microcode updates
  hardware.cpu.amd.updateMicrocode = true;

  # Configure network proxy if necessary
  # networking.proxy.default = "http://user:password@proxy:port/";
  # networking.proxy.noProxy = "127.0.0.1,localhost,internal.domain";

  # Select internationalisation properties.
  # i18n.defaultLocale = "en_US.UTF-8";
  # console = {
  #   font = "Lat2-Terminus16";
  #   keyMap = "us";
  #   useXkbConfig = true; # use xkb.options in tty.
  # };
  
  # KeyBase
  services.keybase.enable = true;
  services.kbfs.enable = true;

  # Mount usb disks automatically
  services.udisks2.enable = true;
  services.devmon.enable = true;
  services.gvfs.enable = true;

  # Enable the X11 windowing system.
#  services.xserver.enable = true;
#  services.xserver.displayManager.gdm.enable = true;
#  #services.xserver.desktopManager.gnome.enable = true;
#  services.xserver.windowManager.xmonad = {
#    enable = true;
#    enableContribAndExtras = true;
#  };
  
  # Configure keymap in X11
  # services.xserver.xkb.layout = "us";
  # services.xserver.xkb.options = "eurosign:e,caps:escape";  

  # Enable CUPS to print documents.
  services.printing.enable = true;
  services.printing.drivers = [ pkgs.brlaser ];

  # Enable sound.
  sound.enable = true;
  hardware.pulseaudio.enable = true;

  # Enable touchpad support (enabled default in most desktopManager).
  services.xserver.libinput.enable = true;

  # Define a user account. Don't forget to set a password with ‘passwd’.
  users.mutableUsers = false;
  users.users.aeronth = {
    isNormalUser = true;
    extraGroups = [ "docker" "render" "wheel" "sudo" "networkmanager" "vboxusers" ]; # Enable ‘sudo’ for the user.
    hashedPassword = "$2b$05$a3YUw7uiI1de8EBazxN.hOAHHrvgKwkX0397l3NyyDam8fLA1DqEq";
  };
  users.users.backup = {
    isNormalUser = true;
    extraGroups = [ "render" "wheel" "sudo" "networkmanager" "vboxusers" ]; # Enable ‘sudo’ for the user.
    hashedPassword = "$2b$05$hsGpW6EaQKJ1D4oSC0Id1.Y3R1G2V.aq0gRUmiUX0XUJVkMy4n0n2";
  };

  fonts.fontDir.enable = true;
  fonts.packages = with pkgs; [
    ocr-a
    font-awesome
    material-design-icons
  ];

  # Some programs need SUID wrappers, can be configured further or are
  # started in user sessions.
  # programs.mtr.enable = true;
  programs.gnupg.agent = {
    enable = true;
    enableSSHSupport = true;
    pinentryPackage = pkgs.pinentry-curses;
  };

  programs.fuse.userAllowOther = true;

  # List services that you want to enable:
  
  # Virtualisation
  virtualisation.docker.enable = true;

  # Enable the OpenSSH daemon.
  services.openssh.enable = true;

  # Enable service discovery with Avahi
  services.avahi.enable = true;
  
  # Open ports in the firewall.
  # networking.firewall.allowedTCPPorts = [ ... ];
  # networking.firewall.allowedUDPPorts = [ ... ];
  # Or disable the firewall altogether.
  networking.firewall.enable = false;

  # Copy the NixOS configuration file and link it from the resulting system
  # (/run/current-system/configuration.nix). This is useful in case you
  # accidentally delete configuration.nix.
  system.copySystemConfiguration = true;

  # This option defines the first version of NixOS you have installed on this particular machine,
  # and is used to maintain compatibility with application data (e.g. databases) created on older NixOS versions.
  #
  # Most users should NEVER change this value after the initial install, for any reason,
  # even if you've upgraded your system to a new NixOS release.
  #
  # This value does NOT affect the Nixpkgs version your packages and OS are pulled from,
  # so changing it will NOT upgrade your system.
  #
  # This value being lower than the current NixOS release does NOT mean your system is
  # out of date, out of support, or vulnerable.
  #
  # Do NOT change this value unless you have manually inspected all the changes it would make to your configuration,
  # and migrated your data accordingly.
  #
  # For more information, see `man configuration.nix` or https://nixos.org/manual/nixos/stable/options#opt-system.stateVersion .
  system.stateVersion = "24.05"; # Did you read the comment?

}

